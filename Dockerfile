FROM ubuntu:bionic
LABEL maintainer="Daniel Díaz <daniel.diaz@linaro.org>"

ARG DEBIAN_FRONTEND=noninteractive

RUN echo 'locales locales/locales_to_be_generated multiselect C.UTF-8 UTF-8, en_US.UTF-8 UTF-8 ' | debconf-set-selections \
 && echo 'locales locales/default_environment_locale select en_US.UTF-8' | debconf-set-selections \
 && apt-get update && apt-get install -y --no-install-recommends \
	git \
	jq \
	openssh-client \
	shellcheck \
	wget ca-certificates \
	build-essential chrpath cpio curl diffstat file gawk python python3 texinfo \
	locales \
        gnupg \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 \
 && useradd -m -s /bin/bash -u 1000 lkftuser \
 && mkdir -p /poky \
 && chown -R 1000:1000 /poky \
 && wget -q -O /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo \
 && wget -q -O /usr/local/bin/loeb https://gitlab.com/Linaro/lkft/loeb/-/raw/master/loeb \
 && chmod +x /usr/local/bin/*

WORKDIR /poky
USER lkftuser
ENV LANG=en_US.UTF-8

RUN repo init -u https://github.com/mrchapp/lkft-manifest -b dunfell \
 && repo sync
